﻿using System.Collections.Generic;

namespace MySqlDataBuild
{
    public class DataEntity
    {
        public string ip, port, pwd, user;
        public string sqliteDBPath, sqliteGeneratePath;
        public int pathIndex = -1;
        public List<string> paths = new List<string>();
        public int namespaceIndex;
        public bool clearOldFiles, compatible, creatDbPath;
        public int incrementIdType = 4;
    }
}
