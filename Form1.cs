using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace MySqlDataBuild
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            foreach (var item in Persist.I.datas)
                dbcb.Items.Add(item.Key);
            InitData();
        }

        public void InitData()
        {
            pathCb.Items.Clear();
            foreach (var item1 in Persist.I.paths)
                pathCb.Items.Add(item1);
            dbcb.SelectedItem = Persist.I.dbName;
            pathCb.Text = Persist.I.path;
            //pathCb.SelectedIndex = Persist.I.pathIndex;
            iptext.Text = Persist.I.ip;
            porttext.Text = Persist.I.port;
            usertext.Text = Persist.I.user;
            pwdtext.Text = Persist.I.pwd;
            comboBox1.SelectedIndex = Persist.I.namespaceIndex;
            nameSpaceText.Enabled = comboBox1.SelectedIndex == 2;
            checkBox1.Checked = Persist.I.clearOldFiles;
            checkBox2.Checked = Persist.I.compatible;
            checkBox3.Checked = Persist.I.creatDbPath;
            comboBox2.SelectedIndex = Persist.I.incrementIdType;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                pathCb.Text = folderBrowserDialog1.SelectedPath;
                pathCb_Leave(null, null);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                string path;
                if (string.IsNullOrEmpty(pathCb.Text))
                    path = AppDomain.CurrentDomain.BaseDirectory;
                else
                    path = pathCb.Text;
                var files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
                foreach (var file in files)
                    File.Delete(file);
            }
            GenerateCodeHelper.MySqlBuild(dbcb.Text, iptext.Text, porttext.Text, usertext.Text, pwdtext.Text, pathCb.Text, comboBox1.SelectedIndex, nameSpaceText.Text, checkBox3.Checked, checkBox2.Checked, comboBox2.Text);
        }

        private void iptext_TextChanged(object sender, EventArgs e)
        {
            Persist.I.ip = ((TextBox)sender).Text;
            Persist.SaveData();
        }

        private void porttext_TextChanged(object sender, EventArgs e)
        {
            Persist.I.port = ((TextBox)sender).Text;
            Persist.SaveData();
        }

        private void usertext_TextChanged(object sender, EventArgs e)
        {
            Persist.I.user = ((TextBox)sender).Text;
            Persist.SaveData();
        }

        private void pwdtext_TextChanged(object sender, EventArgs e)
        {
            Persist.I.pwd = ((TextBox)sender).Text;
            Persist.SaveData();
        }

        private void comboBox1_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(dbcb.Text))
                return;
            if (Persist.I.datas.ContainsKey(dbcb.Text))
                return;
            dbcb.Items.Add(dbcb.Text);
            Persist.I.dbName = dbcb.Text;
            Persist.I.datas.Add(dbcb.Text, new DataEntity());
            InitData();
            Persist.SaveData();
        }

        private void dbcb_SelectedIndexChanged(object sender, EventArgs e)
        {
            Persist.I.dbName = dbcb.Text;
            InitData();
            Persist.SaveData();
        }

        private void dbcb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (dbcb.SelectedIndex == -1)
                    return;
                Persist.I.datas.Remove(dbcb.Text);
                dbcb.Items.RemoveAt(dbcb.SelectedIndex);
                Persist.SaveData();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                comboBox1_Leave(sender, e);
            }
        }

        private void pathCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            Persist.I.pathIndex = pathCb.SelectedIndex;
            Persist.SaveData();
        }

        private void pathCb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (pathCb.SelectedIndex == -1)
                    return;
                Persist.I.paths.RemoveAt(pathCb.SelectedIndex);
                pathCb.Items.RemoveAt(pathCb.SelectedIndex);
                Persist.I.pathIndex = -1;
                Persist.SaveData();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                pathCb_Leave(sender, e);
            }
        }

        private void pathCb_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(pathCb.Text))
                return;
            if (Persist.I.paths.Contains(pathCb.Text))
                return;
            pathCb.Items.Add(pathCb.Text);
            Persist.I.paths.Add(pathCb.Text);
            Persist.SaveData();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            nameSpaceText.Enabled = comboBox1.SelectedIndex == 2;
            Persist.I.namespaceIndex = comboBox1.SelectedIndex;
            Persist.SaveData();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Persist.I.clearOldFiles = checkBox1.Checked;
            Persist.SaveData();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Persist.I.compatible = checkBox2.Checked;
            Persist.SaveData();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            Persist.I.creatDbPath = checkBox3.Checked;
            Persist.SaveData();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Persist.I.incrementIdType = comboBox2.SelectedIndex;
            Persist.SaveData();
        }
    }
}
