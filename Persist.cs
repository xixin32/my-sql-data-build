using System;
using System.Collections.Generic;
using System.IO;

namespace MySqlDataBuild
{
    public class Persist
    {
        private static Persist i;
        public static Persist I
        {
            get
            {
                if (i == null)
                {
                    i = new Persist();
                    LoadData(ref i);
                }
                return i;
            }
        }

        public string version;
        public string dbName;
        internal string ip
        {
            get => this[dbName].ip;
            set => this[dbName].ip = value;
        }
        internal string port
        {
            get => this[dbName].port;
            set => this[dbName].port = value;
        }
        internal string pwd
        {
            get => this[dbName].pwd;
            set => this[dbName].pwd = value;
        }
        internal string user
        {
            get => this[dbName].user;
            set => this[dbName].user = value;
        }

        internal string sqliteDBPath
        {
            get => this[dbName].sqliteDBPath;
            set => this[dbName].sqliteDBPath = value;
        }
        internal string sqliteGeneratePath
        {
            get => this[dbName].sqliteGeneratePath;
            set => this[dbName].sqliteGeneratePath = value;
        }

        internal int namespaceIndex
        {
            get => this[dbName].namespaceIndex;
            set => this[dbName].namespaceIndex = value;
        }
        internal bool clearOldFiles
        {
            get => this[dbName].clearOldFiles;
            set => this[dbName].clearOldFiles = value;
        }
        internal bool compatible
        {
            get => this[dbName].compatible;
            set => this[dbName].compatible = value;
        }
        internal bool creatDbPath
        {
            get => this[dbName].creatDbPath;
            set => this[dbName].creatDbPath = value;
        }
        internal int incrementIdType
        {
            get => this[dbName].incrementIdType;
            set => this[dbName].incrementIdType = value;
        }

        internal int pathIndex
        {
            get => this[dbName].pathIndex;
            set => this[dbName].pathIndex = value;
        }
        internal string path
        {
            get
            {
                if (string.IsNullOrEmpty(dbName))
                    return string.Empty;
                if (datas.TryGetValue(dbName, out var data))
                    if (pathIndex != -1 & pathIndex < data.paths.Count)
                        return data.paths[pathIndex];
                return string.Empty;
            }
        }
        internal List<string> paths
        {
            get => this[dbName].paths;
            set => this[dbName].paths = value;
        }

        public Dictionary<string, DataEntity> datas = new Dictionary<string, DataEntity>();

        public DataEntity this[string name]
        {
            get
            {
                if (string.IsNullOrEmpty(dbName))
                    return new DataEntity();
                if (datas.TryGetValue(dbName, out var data))
                    return data;
                return new DataEntity();
            }
        }

        public static void LoadData(ref Persist persist)
        {
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "persist.data"))
                return;
            var jsonText = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "persist.data");
            persist = Newtonsoft.Json.JsonConvert.DeserializeObject<Persist>(jsonText);
        }

        public static void SaveData()
        {
            var jsonText = Newtonsoft.Json.JsonConvert.SerializeObject(I);
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "persist.data", jsonText);
        }
    }
}
