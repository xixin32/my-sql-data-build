﻿using System;
using System.Windows.Forms;

namespace MySqlDataBuild
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            dbcb.Text = Persist.I.sqliteDBPath;
            pathCb.Text = Persist.I.sqliteGeneratePath;
            comboBox1.SelectedIndex = Persist.I.namespaceIndex;
            nameSpaceText.Enabled = comboBox1.SelectedIndex == 2;
            comboBox2.SelectedIndex = Persist.I.incrementIdType;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result =  folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK) 
            {
                pathCb.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                dbcb.Text = openFileDialog1.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GenerateCodeHelper.SqliteBuild(dbcb.Text, pathCb.Text, comboBox1.SelectedIndex, nameSpaceText.Text, checkBox3.Checked, checkBox2.Checked, comboBox2.Text);
        }

        private void dbtext_TextChanged(object sender, EventArgs e)
        {
            Persist.I.sqliteDBPath = ((TextBox)sender).Text;
            Persist.SaveData();
        }

        private void path_TextChanged(object sender, EventArgs e)
        {
            Persist.I.sqliteGeneratePath = ((TextBox)sender).Text;
            Persist.SaveData();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            nameSpaceText.Enabled = comboBox1.SelectedIndex == 2;
            Persist.I.namespaceIndex = comboBox1.SelectedIndex;
            Persist.SaveData();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Persist.I.incrementIdType = comboBox2.SelectedIndex;
            Persist.SaveData();
        }
    }
}
