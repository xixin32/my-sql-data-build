using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Data.SQLite;
using System.Windows.Forms;

namespace MySqlDataBuild
{
    internal class GenerateCodeHelper
    {
        public static void MySqlBuild(string dbName, string ip, string port, string user, string pwd, string savePath,
            int nameSpaceMode, string nameSpace, bool createDbDirectory, bool compatibleName, string incrementIdType)
        {
            var assebly = Assembly.Load(App.dllBytes);
            var type = assebly.GetType("MySql.Data.MySqlClient.MySqlConnection");
            var type1 = assebly.GetType("MySql.Data.MySqlClient.MySqlCommand");
            var builder = new MySql.Data.MySqlClient.MySqlConnectionStringBuilder
            {
                Database = dbName,
                Server = ip,
                Port = uint.Parse(port),
                UserID = user,
                Password = pwd,
                CharacterSet = "utf8mb4",
                Pooling = true,
                UseCompression = true,
                ConnectionTimeout = 60,
            };
            var connBuilderCode = $@"Database = ""{dbName}"",
            Server = ""{ip}"",
            Port = {port},
            UserID = ""{user}"",
            Password = ""{pwd}"",
            CharacterSet = ""utf8mb4"",
            Pooling = true,
            UseCompression = true,
            ConnectionTimeout = 60,
            {(App.version == 1 ? "AllowLoadLocalInfile = true" : "")}";
            var connStr = builder.ToString();
            var connect = (DbConnection)Activator.CreateInstance(type, new object[] { connStr });
            connect.Open();
            var cmd = (DbCommand)Activator.CreateInstance(type1);

            var queryTableAllcmdText = $"select table_name from information_schema.tables where table_schema='{dbName}'";

            BuildNew(dbName, connect, cmd, nameSpaceMode, nameSpace, builder, savePath, createDbDirectory, compatibleName,
                queryTableAllcmdText, 0, "MySql.Data.MySqlClient", "MySqlParameter", "MySqlConnection", "MySqlCommand", "MySqlTransaction", "conn.Ping();//长时间没有连接后断开连接检查状态",
                $@"var conn = PopConnect();
                try
                {{
                    var path = AppDomain.CurrentDomain.BaseDirectory + ""MySqlBulkLoader\\"";
                    var fileName = path + $""{{tableName}}.txt"";
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    using (var stream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                    {{
                        stream.SetLength(0);
                        var text = updateCmdText.ToString();
                        var bytes = Encoding.UTF8.GetBytes(text);
                        stream.Write(bytes, 0, bytes.Length);
                        stream.Flush();
                    }}
                    var bulkLoader = new MySqlBulkLoader(conn)
                    {{
                        TableName = $""`{{tableName}}`"",
                        FieldTerminator = ""|"",
                        LineTerminator = ""\r\n"",
                        NumberOfLinesToSkip = 0,
                        FileName = fileName,
                        EscapeCharacter = '\\',
                        Local = true,
                        CharacterSet = ""utf8mb4"",
                        ConflictOption = MySqlBulkLoaderConflictOption.Replace,
                    }};
                    var stopwatch = Stopwatch.StartNew();
                    var rowCount = bulkLoader.Load();
                    QueryCount += rowCount;
                    stopwatch.Stop();
                    if (rowCount > 2000) NDebug.Log($""SQL批处理完成:{{rowCount}} 用时:{{stopwatch.Elapsed}}"");
                }}
                catch (Exception ex)
                {{
                    NDebug.LogError(""批量错误: 1.如果表字段更改则需要重新生成! 2.打开Navicat菜单工具->命令列界面,输入SHOW VARIABLES LIKE 'local_infile';后回车,看是否开启批量加载!如果没有则输入SET GLOBAL local_infile = ON;回车设置批量加载! 详细信息:"" + ex);
                }}
                finally
                {{
                    conns.Push(conn);
                }}", incrementIdType, connBuilderCode);
        }

        public static void SqliteBuild(string dbPath, string savePath,
            int nameSpaceMode, string nameSpace, bool createDbDirectory, bool compatibleName, string incrementIdType)
        {
            var assebly = typeof(SQLiteConnection).Assembly;
            var type = assebly.GetType("System.Data.SQLite.SQLiteConnection");
            var type1 = assebly.GetType("System.Data.SQLite.SQLiteCommand");
            var dbName = Path.GetFileNameWithoutExtension(dbPath);
            var builder = new SQLiteConnectionStringBuilder
            {
                DataSource = dbPath,
            };
            var connBuilderCode = $@"Database = ""{dbName}"",
            DataSource = ""{dbPath}""";
            var connStr = builder;
            var connect = (DbConnection)Activator.CreateInstance(type, new object[] { connStr });
            connect.Open();
            var cmd = (DbCommand)Activator.CreateInstance(type1);

            var queryTableAllcmdText = $"SELECT name FROM sqlite_master where type='table' order by name";

            BuildNew(dbName, connect, cmd, nameSpaceMode, nameSpace, connStr, savePath, createDbDirectory, compatibleName,
                queryTableAllcmdText, 1, "System.Data.SQLite", "SQLiteParameter", "SQLiteConnection", "SQLiteCommand", "SQLiteTransaction", "",
                @"var stopwatch = Stopwatch.StartNew();
                var rowCount = ExecuteNonQuery(updateCmdText.ToString());
                stopwatch.Stop();
                if (rowCount > 2000) NDebug.Log($""SQL批处理完成:{rowCount} 用时:{stopwatch.Elapsed}"");", incrementIdType, connBuilderCode);
        }

        private static void BuildNew(string dbName, DbConnection connect, DbCommand cmd, int nameSpaceMode, string nameSpace,
            DbConnectionStringBuilder connStr, string savePath, bool createDbDirectory, bool compatibleName, string queryTableAllcmdText, int sqlType,
            string usingName, string sqlParameter, string sqlConnect, string sqlCmd, string sqlTransaction, string ping,
            string batching, string incrementIdType, string connBuilderCode)
        {
            var dbUpp = dbName.ToUpperFirst();
            var dbTable = new DataTable();
            cmd.CommandText = queryTableAllcmdText;
            cmd.Connection = connect;
            using (var sdr = cmd.ExecuteReader())
            {
                dbTable.Load(sdr);
            }
            var createStatement = string.Empty;
            if (sqlType == 0)
            {
                cmd.CommandText = $"SHOW CREATE DATABASE `{dbName}`";
                cmd.Connection = connect;
                using (var sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        createStatement = sdr.GetString(1);
                    }
                }
            }
            var tableNames = new List<StringEntiy>();
            foreach (DataRow row in dbTable.Rows)
            {
                var des = row[0].ToString();
                var des1 = des.ToUpperFirst();
                tableNames.Add(new StringEntiy(des, des1));
            }
            var rpcMaskDic = new Dictionary<string, Dictionary<string, string[]>>();
            var createTableSqls = new List<DataRow>();
            foreach (var tableName in tableNames)
            {
                var table = new DataTable();
                var table1 = new DataTable();
                var table2 = new DataTable();
                cmd.CommandText = $"SELECT * FROM `{tableName.source}` limit 0;";
                cmd.Connection = connect;
                using (var sdr = cmd.ExecuteReader())
                {
                    table.Load(sdr);
                }
                if (sqlType == 0)
                {
                    cmd.CommandText = $"select COLUMN_NAME,column_comment,character_maximum_length from INFORMATION_SCHEMA.Columns where table_name='{tableName.source}' and table_schema='{dbName}'";
                    cmd.Connection = connect;
                    using (var sdr = cmd.ExecuteReader())
                    {
                        table1.Load(sdr);
                    }
                    cmd.CommandText = $"SHOW CREATE TABLE `{tableName.source}`";
                    cmd.Connection = connect;
                    using (var sdr = cmd.ExecuteReader())
                    {
                        table2.Load(sdr);
                    }
                }
                else
                {
                    cmd.CommandText = $"PRAGMA table_info(`{tableName.source}`);";
                    cmd.Connection = connect;
                    using (var sdr = cmd.ExecuteReader())
                    {
                        table1.Load(sdr);
                    }
                }
                var codeText = Properties.Resources.tableTemplate;
                if (nameSpaceMode == 1)
                {
                    codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {dbUpp}\r\n{{");
                    codeText = codeText.Replace("{NAMESPACE_END}", "}");
                }
                else if (nameSpaceMode == 2)
                {
                    codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {nameSpace}\r\n{{");
                    codeText = codeText.Replace("{NAMESPACE_END}", "}");
                }
                else
                {
                    codeText = codeText.Replace("{NAMESPACE_BEGIN}", "");
                    codeText = codeText.Replace("{NAMESPACE_END}", "");
                }
                codeText = codeText.Replace("{TYPENAME}", $"{tableName.newString}Data");
                if (sqlType == 1)
                    codeText = codeText.Replace("bool isBulk = true", "bool isBulk = false");
                var pkeys = table.PrimaryKey;
                if (pkeys.Length == 0)
                    throw new Exception($"{table.TableName}表必须设置Key, 每个表必须设置一个Key");
                tableName.key = pkeys[0].ColumnName;
                codeText = codeText.Replace("{KETTYPE}", $"{GetCodeType(pkeys[0].DataType)}");
                codeText = codeText.Replace("{KETTYPETRUE}", $"{pkeys[0].DataType.FullName}");
                var colName1 = pkeys[0].ColumnName.ToLowerFirst();
                var colName2 = pkeys[0].ColumnName.ToUpperFirst();
                codeText = codeText.Replace("{KEYNAME1}", $"{colName1}");
                codeText = codeText.Replace("{KEYNAME2}", $"{colName2}");
                codeText = codeText.Replace("{TABLENAME}", $"{tableName.source}");
                codeText = codeText.Replace("{DBNAME}", $"{dbUpp}DB");
                codeText = codeText.Replace("{COUNT}", $"{table.Columns.Count}");
                codeText = codeText.Replace("{PARAMETER}", sqlParameter);
                codeText = codeText.Replace("{CLIENT}", $"{dbUpp}DBEvent");

                var codeTexts = codeText.Split(new string[] { "[split]" }, 0);

                var dic = new Dictionary<string, string[]>();
                if (sqlType == 0)
                {
                    foreach (DataRow item in table1.Rows)
                    {
                        var length = item.ItemArray[2].ToString();
                        if (string.IsNullOrEmpty(length)) length = "0";
                        dic[item.ItemArray[0].ToString()] = new string[] { item.ItemArray[1].ToString(), length };
                    }
                }
                else
                {
                    foreach (DataRow item in table1.Rows)
                    {
                        var length = item.ItemArray[2].ToString();
                        if (length.EndsWith(")"))
                        {
                            var index = length.IndexOf("(");
                            length = length.Remove(0, index + 1);
                            length = length.TrimEnd(')');
                        }
                        else
                        {
                            length = "65535";
                        }
                        if (string.IsNullOrEmpty(length)) length = "0";
                        dic[item.ItemArray[1].ToString()] = new string[] { item.ItemArray[1].ToString(), length };
                    }
                }

                foreach (DataRow item in table2.Rows)
                {
                    createTableSqls.Add(item);
                }

                codeTexts[0] = codeTexts[0].Replace("{KEYNOTE}", $"{dic[pkeys[0].ColumnName][0]}");
                codeTexts[0] = codeTexts[0].Replace("{USING}", usingName);
                codeTexts[0] = codeTexts[0].Replace("{RPCHASHTYPE}", $"{dbUpp}HashProto");

                var sb_code = new StringBuilder(codeTexts[0]);
                var sb_no3 = new StringBuilder();
                var sb_no5 = new StringBuilder();
                var sb_no9 = new StringBuilder();
                var sb_no7 = new StringBuilder();
                var sb_no11 = new StringBuilder();
                var sb_no13 = new StringBuilder();
                var sb_no15 = new StringBuilder();

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    var cell = table.Columns[i];
                    if (char.IsUpper(cell.ColumnName[0]))
                    {
                        MessageBox.Show($"表:{table.TableName}的列:{cell.ColumnName} 出错! 列名前缀不能使用大写!");
                        return;
                    }
                    var isKey = cell == pkeys[0];

                    var cellName = cell.ColumnName;
                    var fieldName1 = cellName.ToLowerFirst();
                    var fieldName2 = cellName.ToUpperFirst();

                    var indexCode = codeTexts[3].Replace("{INDEX}", $"{i}");
                    indexCode = indexCode.Replace("{INDEXNAME}", $"{fieldName1}");
                    indexCode = indexCode.Replace("{TEXTLENGTH}", $"{dic[cell.ColumnName][1]}");
                    sb_no3.Append(indexCode);

                    var indexCode1 = codeTexts[5].Replace("{INDEX}", $"{i}");
                    indexCode1 = indexCode1.Replace("{INDEXNAME}", $"{(isKey ? fieldName1 : $"{fieldName1}.Value")}");
                    sb_no5.Append(indexCode1);

                    var indexCode11 = codeTexts[9].Replace("{FIELDNAME1}", $"\"{fieldName1}\"");
                    indexCode11 = indexCode11.Replace("{INDEXNAME}", $"{(isKey ? fieldName1 : $"{fieldName1}.Value")}");
                    sb_no9.Append(indexCode11);

                    bool isArray = cell.DataType == typeof(byte[]);
                    var isAtk = !isArray & cell.DataType != typeof(bool) & cell.DataType != typeof(string) & cell.DataType != typeof(DateTime);
                    var indexCode2 = codeTexts[7].Replace("{INDEX}", $"{i}");
                    if (isKey)
                        indexCode2 = indexCode2.Replace("{ADDORCUTROW}", $"this.{fieldName1} = ({GetCodeType(cell.DataType)})value;");
                    else
                        indexCode2 = indexCode2.Replace("{ADDORCUTROW}", $"Check{fieldName2}{(isArray ? "Bytes" : "")}Value(({GetCodeType(cell.DataType)})Convert.ChangeType(value, typeof({GetCodeType(cell.DataType)})), -1);");
                    sb_no7.Append(indexCode2);

                    var indexCode22 = codeTexts[11].Replace("{FIELDNAME1}", $"\"{fieldName1}\"");
                    if (isKey)
                        indexCode22 = indexCode22.Replace("{ADDORCUTROW}", $"this.{fieldName1} = ({GetCodeType(cell.DataType)})value;");
                    else
                        indexCode22 = indexCode22.Replace("{ADDORCUTROW}", $"Check{fieldName2}{(isArray ? "Bytes" : "")}Value(({GetCodeType(cell.DataType)})Convert.ChangeType(value, typeof({GetCodeType(cell.DataType)})), -1);");
                    sb_no11.Append(indexCode22);

                    var indexCode4 = codeTexts[13].Replace("{INDEX}", $"{i}");
                    indexCode4 = indexCode4.Replace("{FIELDTYPE}", $"{GetCodeType(cell.DataType)}");
                    indexCode4 = indexCode4.Replace("{FIELDTYPE1}", $"{GetCodeType1(cell.DataType)}Obs");
                    indexCode4 = indexCode4.Replace("{FIELDNAME1}", $"{fieldName1}");
                    indexCode4 = indexCode4.Replace("{INIT}", isKey ? $"this.{fieldName1} = {fieldName1};" : $"Check{fieldName2}{(isArray ? "Bytes" : "")}Value({(isArray ? $"Convert.FromBase64String(Encoding.ASCII.GetString({fieldName1}))" : $"{fieldName1}")}, -1);");
                    sb_no13.Append(indexCode4);

                    sb_no15.Append($"{fieldName2}:{{{fieldName2}{(isArray ? "Bytes" : "")}}} ");

                    if (isKey)
                        continue;

                    var rpcenumName = (table.TableName + "_" + cell.ColumnName).ToUpperAll();
                    if (!rpcMaskDic.TryGetValue(table.TableName, out var dict))
                        rpcMaskDic.Add(table.TableName, dict = new Dictionary<string, string[]>());
                    dict.Add(rpcenumName, dic[cell.ColumnName]);

                    var fieldCode = codeTexts[1].Replace("{FIELDTYPE}", $"{GetCodeType(cell.DataType)}");
                    fieldCode = fieldCode.Replace("{FIELDTYPE1}", $"{GetCodeType1(cell.DataType)}Obs");
                    fieldCode = fieldCode.Replace("{FIELDTYPETRUE}", $"{cell.DataType.FullName}");
                    fieldCode = fieldCode.Replace("{FIELDNAME1}", $"{fieldName1}");
                    fieldCode = fieldCode.Replace("{FIELDNAME2}", $"{fieldName2}{(isArray ? "Bytes" : "")}");
                    fieldCode = fieldCode.Replace("{KEYNAME1}", $"{colName1}");
                    fieldCode = fieldCode.Replace("{PUBLIC}", $"{(isArray ? "internal" : "public")}");
                    fieldCode = fieldCode.Replace("{INDEX}", i.ToString());

                    fieldCode = fieldCode.Replace("{VARSET}", isArray ? $"object bytes = {fieldName1}.Value;" : "");
                    fieldCode = fieldCode.Replace("{VARSET1}", (isKey ? fieldName1 : isArray ? $"bytes" : $"{fieldName1}.Value"));

                    fieldCode = fieldCode.Replace("{ISATK}", isAtk.ToString().ToLower());

                    fieldCode = fieldCode.Replace("{RPCHASHTYPE}", $"{dbUpp}HashProto");
                    fieldCode = fieldCode.Replace("{RPCHASHVALUE}", $"{rpcenumName}");
                    fieldCode = fieldCode.Replace("{NOTE}", $"{dic[cell.ColumnName][0]}");
                    fieldCode = fieldCode.Replace("{NOTE1}", $"{dic[cell.ColumnName][0]} --同步到数据库");
                    fieldCode = fieldCode.Replace("{NOTE2}", $"{dic[cell.ColumnName][0]} --同步带有Key字段的值到服务器Player对象上，需要处理");
                    fieldCode = fieldCode.Replace("{NOTE3}", $"{dic[cell.ColumnName][0]} --同步当前值到服务器Player对象上，需要处理");
                    fieldCode = fieldCode.Replace("{NOTE4}", $"{dic[cell.ColumnName][0]} --获得属性观察对象");

                    fieldCode = fieldCode.Replace("{JUDGE}", isArray ? "" : $"if (this.{fieldName1}.Value == value)\r\n                return;");

                    sb_code.Append(fieldCode);
                }

                codeTexts[15] = codeTexts[15].Replace("{TOSTRING}", sb_no15.ToString());

                sb_code.Append(codeTexts[2]);
                sb_code.Append(sb_no3);
                sb_code.Append(codeTexts[4]);
                sb_code.Append(sb_no5);
                sb_code.Append(codeTexts[6]);
                sb_code.Append(sb_no7);
                sb_code.Append(codeTexts[8]);
                sb_code.Append(sb_no9);
                sb_code.Append(codeTexts[10]);
                sb_code.Append(sb_no11);
                sb_code.Append(codeTexts[12]);
                sb_code.Append(sb_no13);
                sb_code.Append(codeTexts[14]);
                sb_code.Append(codeTexts[15]);

                var codeText1 = sb_code.ToString();

                string path;
                if (string.IsNullOrEmpty(savePath))
                    path = AppDomain.CurrentDomain.BaseDirectory;
                else
                    path = savePath + "\\";
                if (createDbDirectory)
                    path += dbUpp + "DB";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                path += "\\" + (compatibleName ? dbUpp + "_" : "") + tableName.newString + "Data.cs";
                File.WriteAllText(path, sb_code.ToString());
            }
            BuildDBNew(dbUpp, tableNames, rpcMaskDic, createTableSqls, createStatement, connStr, nameSpaceMode, nameSpace, savePath,
                usingName, sqlConnect, sqlCmd, sqlTransaction, ping, batching, incrementIdType, connBuilderCode);
        }

        private static string GetCodeType(Type type)
        {
            var code = Type.GetTypeCode(type);
            if (code != TypeCode.Object)
                return code.ToString();
            return type.FullName;
        }

        private static string GetCodeType1(Type type)
        {
            var code = Type.GetTypeCode(type);
            if (code != TypeCode.Object)
                return code.ToString();
            if (type == typeof(byte[]))
                return "Bytes";
            return type.FullName;
        }

        private static void BuildDBNew(string db, List<StringEntiy> tableNames, Dictionary<string, Dictionary<string, string[]>> rpcMaskDic,
            List<DataRow> createTableSqls, string createStatement, DbConnectionStringBuilder connStr, int nameSpaceMode,
            string nameSpace, string savePath, string usingName, string sqlConnect, string sqlCmd, string sqlTransaction, string ping,
            string batching, string incrementIdType, string connBuilderCode)
        {
            var sb1 = new StringBuilder();
            sb1.AppendLine($"public enum {db}HashProto : ushort");
            sb1.AppendLine("{");
            int index = 4096;
            foreach (var item in rpcMaskDic)
            {
                bool f = false;
                foreach (var item1 in item.Value)
                {
                    sb1.AppendLine($"    /// <summary>{item1.Value[0]}</summary>");
                    sb1.AppendLine($"    {item1.Key}{(f ? "" : $" = {index}")},");
                    f = true;
                }
                index += 1000;
            }
            sb1.AppendLine("}");

            var codeText15 = $@"NAMESPACE_BEGIN
    public class {db}DBEvent
    {{
        private static Net.Client.ClientBase client;
        /// <summary>
        /// 设置同步到服务器的客户端对象, 如果不设置, 则默认是ClientBase.Instance对象
        /// </summary>
        public static Net.Client.ClientBase Client
        {{
            get
            {{
                if (client == null)
                    client = Net.Client.ClientBase.Instance;
                return client;
            }}
            set => client = value;
        }}

        /// <summary>
        /// 当服务器属性同步给客户端, 如果需要同步属性到客户端, 需要监听此事件, 并且调用发送给客户端
        /// 参数1: 要发送给哪个客户端
        /// 参数2: cmd
        /// 参数3: methodHash
        /// 参数4: pars
        /// </summary>
        public static System.Action<Net.Server.NetPlayer, byte, ushort, object[]> OnSyncProperty;

        SYNC_KEY
    }}
NAMESPACE_END";

            var incrementIDTypeCode = $@"#if SERVER
NAMESPACE_BEGIN
    public partial class {db}IncrementIdType
    {{
        {{FIELD}}
    }}
NAMESPACE_END
#endif
";

            var db1 = db + "DB";
            var codeText = Properties.Resources.databaseTemplate;
            if (nameSpaceMode == 1)
            {
                codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {db}\r\n{{");
                codeText = codeText.Replace("{NAMESPACE_END}", "}");
                codeText15 = codeText15.Replace("NAMESPACE_BEGIN", $"namespace {db}\r\n{{");
                codeText15 = codeText15.Replace("NAMESPACE_END", "}");
                incrementIDTypeCode = incrementIDTypeCode.Replace("NAMESPACE_BEGIN", $"namespace {db}\r\n{{");
                incrementIDTypeCode = incrementIDTypeCode.Replace("NAMESPACE_END", "}");
            }
            else if (nameSpaceMode == 2)
            {
                codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {nameSpace}\r\n{{");
                codeText = codeText.Replace("{NAMESPACE_END}", "}");
                codeText15 = codeText15.Replace("NAMESPACE_BEGIN", $"namespace {nameSpace}\r\n{{");
                codeText15 = codeText15.Replace("NAMESPACE_END", "}");
                incrementIDTypeCode = incrementIDTypeCode.Replace("NAMESPACE_BEGIN", $"namespace {nameSpace}\r\n{{");
                incrementIDTypeCode = incrementIDTypeCode.Replace("NAMESPACE_END", "}");
            }
            else
            {
                codeText = codeText.Replace("{NAMESPACE_BEGIN}", "");
                codeText = codeText.Replace("{NAMESPACE_END}", "");
                codeText15 = codeText15.Replace("NAMESPACE_BEGIN", "");
                codeText15 = codeText15.Replace("NAMESPACE_END", "");
                incrementIDTypeCode = incrementIDTypeCode.Replace("NAMESPACE_BEGIN", "");
                incrementIDTypeCode = incrementIDTypeCode.Replace("NAMESPACE_END", "");
            }
            codeText = codeText.Replace("{DBNAME}", $"{db1}");
            codeText = codeText.Replace("{DBNAME1}", $"{db}");
            codeText = codeText.Replace("{USING}", usingName);
            codeText = codeText.Replace("{CONNECT}", sqlConnect);
            codeText = codeText.Replace("{COMMAND}", sqlCmd);
            codeText = codeText.Replace("{TRANSACTION}", sqlTransaction);
            codeText = codeText.Replace("{PING}", ping);
            codeText = codeText.Replace("{DATABASENAME}", db);
            codeText = codeText.Replace("{BATCHING}", batching);
            codeText = codeText.Replace("{INCREMENTIDTYPE}", incrementIdType);
            codeText = codeText.Replace("{CONNECTIONBUILDERTYPE}", connStr.GetType().Name);
            codeText = codeText.Replace("{CONNECTIONBUILDERFIELD}", connBuilderCode);

            var codeTexts = codeText.Split(new string[] { "[split]" }, 0);

            var sb = new StringBuilder(codeTexts[0]);
            var sb_no1 = new StringBuilder();
            var sb_no2 = new StringBuilder();
            var sb3 = new StringBuilder();
            var sb_no5 = new StringBuilder();

            var sb_no6 = new StringBuilder();

            for (int i = 0; i < tableNames.Count; i++)
            {
                var tableName = tableNames[i];
                var indexCode = codeTexts[1].Replace("{TABLENAME}", $"{tableName.source}");
                indexCode = indexCode.Replace("{TABLENAME1}", $"{tableName.newString}");
                sb_no1.Append(indexCode);
                sb3.Append($"/// <summary>{tableName.newString}Data类对象属性同步id索引</summary>\r\n\t\tpublic static int {tableName.newString}Data_SyncID = 0;\r\n\t\t");
                sb_no2.AppendLine($"{(sb_no2.Length == 0 ? "" : "            ")}IncrementIdMap[{db}IncrementIdType.{tableName.newString}] = ExecuteScalar<{incrementIdType}>(@\"SELECT MAX({tableName.key}) FROM `{tableName.source}`;\");");
                sb_no6.AppendLine($"{(sb_no6.Length == 0 ? "" : "        ")}public const short {tableName.newString} = {i + 1};");
            }

            codeTexts[2] = codeTexts[2].Replace("{INCREMENTIDMAP}", sb_no2.ToString());
            incrementIDTypeCode = incrementIDTypeCode.Replace("{FIELD}", sb_no6.ToString());

            foreach (var item in createTableSqls)
            {
                var text = codeTexts[5].Replace("{TABLENAME}", item.ItemArray[0].ToString());
                text = text.Replace("{DATABASENAME}", db);
                text = text.Replace("{CREATETABLESQL}", item.ItemArray[1].ToString());
                sb_no5.Append(text);
            }

            codeTexts[4] = codeTexts[4].Replace("{CREATEDATABASESQL}", createStatement);

            sb.Append(sb_no1);
            sb.Append(codeTexts[2]);
            sb.Append(codeTexts[3]);
            sb.Append(codeTexts[4]);
            sb.Append(sb_no5);
            sb.Append(codeTexts[6]);

            codeText15 = codeText15.Replace("SYNC_KEY", sb3.ToString());

            string path, path1, path2, path3;
            if (string.IsNullOrEmpty(savePath))
            {
                path = AppDomain.CurrentDomain.BaseDirectory + db1 + ".cs";
                path1 = AppDomain.CurrentDomain.BaseDirectory + $"{db}HashProto.cs";
                path2 = AppDomain.CurrentDomain.BaseDirectory + $"{db}DBEvent.cs";
                path3 = AppDomain.CurrentDomain.BaseDirectory + $"{db}IncrementIdType.cs";
            }
            else
            {
                path = savePath + "/" + db1 + ".cs";
                path1 = savePath + "/" + $"{db}HashProto.cs";
                path2 = savePath + "/" + $"{db}DBEvent.cs";
                path3 = savePath + "/" + $"{db}IncrementIdType.cs";
            }
            File.WriteAllText(path, sb.ToString());
            File.WriteAllText(path1, sb1.ToString());
            File.WriteAllText(path2, codeText15);
            File.WriteAllText(path3, incrementIDTypeCode);

            MessageBox.Show("生成成功!");
        }

    }
}
